//
// Created by robert on 19.5.22.
//

#ifndef APOSEM_DEFINES_H
#define APOSEM_DEFINES_H

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>

#define WIDTH 480   // x
#define HEIGHT 320  // y
#define SQ_LEN 50
#define MAX_ROW 6
#define MAX_COL 7
#define BOARD_SIZE 42

#define RED 0xF800
#define YELLOW 0xFFE0
#define GREEN 0x07E0
#define BLUE 0x00FF
#define BLACK 0x0000
#define WHITE 0xFFFF

#define LED_RED 0xff0000
#define LED_GREEN 0x00ff00
#define LED_BLUE 0x0000ff
#define LED_YELLOW 0xffcc00
#define LED_PINK 0xcc00cc
#define LED_BLACK 0x000000

#endif //APOSEM_DEFINES_H
