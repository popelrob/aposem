//
// Created by robert on 17.5.22.
//

#ifndef APOSEM_GRAF_ENGINE_H
#define APOSEM_GRAF_ENGINE_H

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"
#include "defines.h"

void draw_display();
// refrsh the lcd display with the current contents of the display buffer
void color_and_draw_display(uint16_t color);
// color the whole display with a specific color
void write_pixel(int x, int y, uint16_t color);
// changes the value of a pixel specified by x y coordinates in the display buffer
void write_char(int x, int y, char ch, uint16_t color);
// writes a specified character on the specified x y position in the display buffer
void write_str(int x, int y, char *str, uint16_t color);
// writes a specified string on the specified x y position in the display buffer
int coord(int row, int col);
// returns position in the buffer based on x y coordinates
void write_square(int x, int y, uint16_t color);
// writes a square with a specified color in the display buffer
void write_chosen_square(int x, int y, uint16_t color, bool rewrite);
// marks a specified square on the playing field
void draw_menu();
// renders the main menu
void draw_win(char player);
// renders the victory screen
void draw_quit(char player);
// renders the quit screen
void draw_draw();
// renders the draw screen
void draw_arrow(uint8_t pos);
// renders the main menu arrow pointer
void paint_field();
// renders the playing field
void paint_token(const char *game_state);
// renders the tokens on the playing field
void display_init();
// initialize the display
int char_width(int ch);
// returns the width of a character

#endif //APOSEM_GRAF_ENGINE_H
