//
// Created by robert on 7.5.22.
//
#include "pvp_module.h"

bool play_turn(char *game_state, char player_turn, uint8_t *col_buff) {
    uint8_t col_pointer;    // pointer to a column selected by either player
    uint8_t knob_pressed;   // keeps track of which knob is currently pressed
    bool btn_select = false;
    bool valid_turn = false;
    while (!valid_turn) {
        while (!btn_select) {   // red knob -> player x choosing, green knob -> player o choosing
            col_pointer = player_turn == 'x' ? - 1 * ((red_knob_turn() % 7) - 6) : - 1 * ((green_knob_turn() % 7) - 6);
            if (col_buff[col_pointer] == 0) {
                // color the selected square blue to signal full column
                write_chosen_square(col_pointer, col_buff[col_pointer], BLUE, true);
                change_led_color(player_turn == 'x' ? LED_RED : LED_GREEN);
            } else {
                // color the selected square and the rgb LED diodes with the current player color
                write_chosen_square(col_pointer, (col_buff[col_pointer] - 1), player_turn == 'x' ? RED : GREEN, true);
                change_led_color(player_turn == 'x' ? LED_RED : LED_GREEN);
            }
            knob_pressed = is_knob_pressed();
            switch (knob_pressed) {
                case 0:
                    break;
                case 1:
                    btn_select = player_turn == 'x' ? true : false;
                    break;
                case 2:
                    btn_select = player_turn == 'o' ? true : false;
                    break;
                case 3:
                    return false;
                default:
                    break;
            }
        }
        if (col_buff[col_pointer] > 0) {    // check if move is valid
            game_state[MAX_COL * (col_buff[col_pointer] - 1) + col_pointer] = player_turn;
            valid_turn = true;
            col_buff[col_pointer]--;
            break;
        } else {
            btn_select = false;
            change_led_color(LED_PINK);     // if not valid turn signal this with rgb LED diodes
            sleep(1);
        }
    }
    return true;
}

bool check_four(const char *game_state, int a, int b, int c, int d){
    if (game_state[a] == game_state[b] && game_state[b] == game_state[c] && game_state[c] == game_state[d] && game_state[a] != 0) {
        change_led_color(LED_YELLOW);   // in case of win color selected squares and rgb LED diodes yellow
        write_chosen_square((int)(a % MAX_COL), (int)(a / MAX_COL), YELLOW, false);
        write_chosen_square((int)((b) % MAX_COL), (int)((b) / MAX_COL), YELLOW, false);
        write_chosen_square((int)((c) % MAX_COL), (int)((c) / MAX_COL), YELLOW, false);
        write_chosen_square((int)((d) % MAX_COL), (int)((d) / MAX_COL), YELLOW, false);
        return true;
    } else {
        return false;
    }
}

bool horizontal_check(char *game_state){
    int row, col, idx;
    const int width = 1;

    for(row = 0; row < MAX_ROW; row++){
        for(col = 0; col < MAX_COL - 3; col++){
            idx = MAX_COL * row + col;
            if(check_four(game_state, idx, idx + width, idx + width * 2, idx + width * 3)){
                return true;
            }
        }
    }
    return false;
}

bool vertical_check(char *game_state){
    int row, col, idx;
    const int height = MAX_COL;

    for(row = 0; row < MAX_ROW - 3; row++){
        for(col = 0; col < MAX_COL; col++){
            idx = MAX_COL * row + col;
            if(check_four(game_state, idx, idx + height, idx + height * 2, idx + height * 3)){
                return true;
            }
        }
    }
    return false;
}

bool diagonal_check(char *game_state){
    int row, col, idx, count = 0;
    const int diag_rgt = 6, diag_lft = 8;

    for(row = 0; row < MAX_ROW - 3; row++){
        for(col = 0; col < MAX_COL; col++){
            idx = MAX_COL * row + col;
            if(count <= 3 && check_four(game_state, idx, idx + diag_lft, idx + diag_lft * 2, idx + diag_lft * 3)
            || count >= 3 && check_four(game_state, idx, idx + diag_rgt, idx + diag_rgt * 2, idx + diag_rgt * 3)){
                return true;
            }
            count++;
        }
        count = 0;
    }
    return false;
}

bool check_win(char *game_state) {
    return (horizontal_check(game_state) || vertical_check(game_state) || diagonal_check(game_state));
}

bool pvp_engine() {
    sleep(1);
    uint8_t col_buff[7] = {6,6,6,6,6,6,6};
    bool game_end = false;
    bool quit = false;
    char player_turn = 'x';
    char game_state[BOARD_SIZE];        // create playing board array
    memset(game_state, 0, sizeof(char) * BOARD_SIZE);      // 0 -> empty tile, o -> player o, x -> player x
    paint_field();

    while (!game_end && !quit) {    // check for draw
        for (int i = 0; i < 7; ++i) {
            if (col_buff[i] > 0) {
                break;
            } else if (i == 6) {
                change_led_color(LED_PINK);
                sleep(1);
                return false;
            }
        }
        switch (player_turn) {
            case 'x':
                if (!play_turn(game_state, player_turn, col_buff)) {
                    quit = true;
                    break;
                }
                game_end = check_win(game_state);
                if (game_end) {
                    break;
                }
                player_turn = 'o';
                break;
            case 'o':
                if (!play_turn(game_state, player_turn, col_buff)) {
                    quit = true;
                    break;
                }
                game_end = check_win(game_state);
                if (game_end) {
                    break;
                }
                player_turn = 'x';
                break;
            default:
                break;
        }
        paint_token(game_state);    // render current game state
    }
    activate_led_line(6);   // signal end of game
    if (game_end) {
        draw_win(player_turn);
    } else {
        draw_quit(player_turn);
    }
    return true;
}
