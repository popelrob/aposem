//
// Created by robert on 7.5.22.
//
#include "graf_engine.h"

struct {
    font_descriptor_t *fdes;
    unsigned char *parlcd_mem_base;
    unsigned char *mem_base;
    int prev_coords[2];
    uint16_t display[HEIGHT * WIDTH];
} lcd = {NULL, NULL, NULL, {-1, -1}};


void display_init() {
    lcd.mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (lcd.mem_base == NULL)
        exit(1);
    lcd.parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (lcd.parlcd_mem_base == NULL)
        exit(1);
    lcd.fdes = &font_winFreeSystem14x16;
    for (int i = 0; i < HEIGHT * WIDTH; ++i) {
        lcd.display[i] = 0;
    }
}

// Pomocne funkci

void draw_display() {
    parlcd_write_cmd(lcd.parlcd_mem_base, 0x2c);
    for (int ptr = 0; ptr < WIDTH * HEIGHT; ptr++) {
        parlcd_write_data(lcd.parlcd_mem_base, lcd.display[ptr]);
    }
}

void color_and_draw_display(uint16_t color) {
    parlcd_write_cmd(lcd.parlcd_mem_base, 0x2c);
    for (int i = 0; i < WIDTH * HEIGHT; i++) {
        lcd.display[i] = color;
        parlcd_write_data(lcd.parlcd_mem_base, lcd.display[i]);
    }
}

void write_pixel(int x, int y, unsigned short color) {
    if (x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT) {
        lcd.display[x + WIDTH * y] = color;
    }
}

void write_char(int x, int y, char ch, uint16_t color) {
    ch -= lcd.fdes->firstchar;
    int w = (!lcd.fdes->width) ? lcd.fdes->maxwidth : lcd.fdes->width[ch];
    uint16_t *pd = lcd.fdes->bits + (ch * lcd.fdes->height);
    for (int j = 0; j < lcd.fdes->height; ++j) {
        uint16_t d = *pd++;
        for (int i = 0; i < w; ++i) {
            if (d & 0x8000) {
                write_pixel(x + 3 * i, y + 3 * j, color);
                write_pixel(x + 3 * i + 1, y + 3 * j, color);
                write_pixel(x + 3 * i, y + 3 * j + 1, color);
                write_pixel(x + 3 * i + 1, y + 3 * j + 1, color);
                write_pixel(x + 3 * i + 2, y + 3 * j, color);
                write_pixel(x + 3 * i, y + 3 * j + 2, color);
                write_pixel(x + 3 * i + 2, y + 3 * j + 1, color);
                write_pixel(x + 3 * i + 1, y + 3 * j + 2, color);
                write_pixel(x + 3 * i + 2, y + 3 * j + 2, color);
            } //else pozadi
            d <<= 1;
        }
    }
}

int char_width(int ch) {
    int width = 0;
    if ((ch >= lcd.fdes->firstchar) && (ch - lcd.fdes->firstchar < lcd.fdes->size)) {
        ch -= lcd.fdes->firstchar;
        if (!lcd.fdes->width) {
            width = lcd.fdes->maxwidth;
        } else {
            width = lcd.fdes->width[ch];
        }
    }
    return width;
}


void write_str(int x, int y, char *str, uint16_t color) {
    while (*str != '\0') {
        write_char(x, y, *str, color);
        x += char_width(*str) * 3;
        str++;
    }
}


int coord(int row, int col) {
    int ret = row * MAX_COL + col;
    return ret;
}

void write_square(int x, int y, uint16_t color) {
    for (int i = 0; i < SQ_LEN; ++i) {
        for (int j = 0; j < SQ_LEN; ++j) {
            int posx = i + x;
            int posy = j + y;
            if (posx <= x + 2 && posx >= x || posy <= y + 2 && posy >= y
                || posx >= x + SQ_LEN - 3 && posx <= x + SQ_LEN - 1 ||
                posy >= y + SQ_LEN - 3 && posy <= y + SQ_LEN - 1) {
                write_pixel(posx, posy, color);
            }
        }
    }
}


void write_chosen_square(int x, int y, uint16_t color, bool rewrite) {
    int posx = 66;
    int posy = 11;
    if (lcd.prev_coords[0] < 0) {
        lcd.prev_coords[0] = x;
        lcd.prev_coords[1] = y;
        write_square(posx + SQ_LEN * x, posy + SQ_LEN * y, color);
        draw_display();
    } else {
        if (rewrite) {
            write_square(posx + SQ_LEN * lcd.prev_coords[0], posy + SQ_LEN * lcd.prev_coords[1], WHITE);
        }
        lcd.prev_coords[0] = x;
        lcd.prev_coords[1] = y;
        write_square(posx + SQ_LEN * x, posy + SQ_LEN * y, color);
        draw_display();
    }
}

void draw_menu() {
    color_and_draw_display(BLACK);
    int top_offset = 46;
    int char_offset = 38;
    write_str(200, top_offset, "PVP", WHITE);
    write_str(200, top_offset + char_offset * 2, "PVE", WHITE);
    write_str(200, top_offset + char_offset * 4, "EXIT", WHITE);
    draw_display();
}

void draw_arrow(uint8_t pos) {
    int top_offset = 46;
    int char_offset = 38;
    write_char(165, top_offset + char_offset * 0, '>', BLACK);
    write_char(165, top_offset + char_offset * 2, '>', BLACK);
    write_char(165, top_offset + char_offset * 4, '>', BLACK);
    write_char(165, top_offset + char_offset * pos * 2, '>', WHITE);
    draw_display();
}

void draw_win(char player) {
    color_and_draw_display(BLACK);
    if (player == 'o') {
        write_str(121, 119, "PLAYER O", GREEN);
        write_str(160, 165, "WINS!", GREEN);
    }else if (player == 'x') {
        write_str(121, 119, "PLAYER X", RED);
        write_str(160, 165, "WINS!", RED);
    }
    draw_display();
}

void draw_quit(char player) {
    color_and_draw_display(BLACK);
    write_str(121, 119, player == 'o' ? "PLAYER O" : "PLAYER X", WHITE);
    write_str(160, 165, "QUIT!", WHITE);
    draw_display();
}

void draw_draw() {
    color_and_draw_display(BLACK);
    write_str(160, 80, "D R A W !", WHITE);
    draw_display();
}

void paint_field() {
    color_and_draw_display(BLACK);
    for (int i = 66; i <= 366; i += SQ_LEN) {
        for (int j = 11; j <= 261; j += SQ_LEN) {
            write_square(i, j, WHITE);
        }
    }
    draw_display();
}

void paint_token(const char *game_state) {
    int x = 77;
    int y = 9;
    for (int i = 0; i < MAX_ROW; ++i) {
        for (int j = 0; j < MAX_COL; ++j) {
            int field = coord(i, j);
            char state = game_state[field];
            if (state == 'o') {
                write_char(x + j * SQ_LEN, y + i * SQ_LEN, state, GREEN);
            }
            if (state == 'x') {
                write_char(x + j * SQ_LEN, y + i * SQ_LEN, state, RED);
            }
        }
    }
    draw_display();
}
