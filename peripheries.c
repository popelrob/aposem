//
// Created by robert on 17.5.22.
//
#include "peripheries.h"

struct {
    int ptr;
    unsigned char *address;
    bool red_pressed;
    bool green_pressed;
    bool blue_pressed;
    uint8_t red;
    uint8_t green;
    uint8_t blue;
}knobs = {0, NULL, false, false, false, 0, 0, 0};


void knobs_init() {
    knobs.address = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    knobs.ptr = *(volatile u_int32_t *)(knobs.address + SPILED_REG_KNOBS_8BIT_o);
    knobs.red = ((knobs.ptr >> 16) & 0xff);
    knobs.green = ((knobs.ptr >> 8) & 0xff);
    knobs.blue = ((knobs.ptr >> 0) & 0xff);
}

uint8_t red_knob_turn() {
    knobs.ptr = *(volatile u_int32_t *)(knobs.address + SPILED_REG_KNOBS_8BIT_o);
    knobs.red = ((knobs.ptr >> 16) & 0xff);
    return (uint8_t)(knobs.red / 10);
}

uint8_t green_knob_turn() {
    knobs.ptr = *(volatile u_int32_t *)(knobs.address + SPILED_REG_KNOBS_8BIT_o);
    knobs.green = ((knobs.ptr >> 8) & 0xff);
    return (uint8_t)(knobs.green / 10);
}

uint8_t blue_knob_turn() {
    knobs.ptr = *(volatile u_int32_t *)(knobs.address + SPILED_REG_KNOBS_8BIT_o);
    knobs.blue = ((knobs.ptr >> 0) & 0xff);
    return (uint8_t)(knobs.blue / 10);
}

uint8_t is_knob_pressed() {
    knobs.ptr = *(volatile u_int32_t *)(knobs.address + SPILED_REG_KNOBS_8BIT_o);
    if ((knobs.ptr >> 26) & 0x1) {
        return 1;
    } else if ((knobs.ptr >> 25) & 0x1) {
        return 2;
    } else if ((knobs.ptr >> 24) & 0x1) {
        return 3;
    } else {
        return 0;
    }
}

void change_led_color(uint32_t color) {
    *(volatile uint32_t*) (knobs.address + SPILED_REG_LED_RGB1_o) = color;
    *(volatile uint32_t*) (knobs.address + SPILED_REG_LED_RGB2_o) = color;
}

void activate_led_line(int rep) {
    for (int j = 0; j < rep; ++j) {
        uint32_t val_line = 255;
        for (int i = 0; i < 24; i++) {
            *(volatile uint32_t*)(knobs.address + SPILED_REG_LED_LINE_o) = val_line;
            val_line <<= 2;
            usleep(10000);
        }
    }
}