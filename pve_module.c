//
// Created by robert on 7.5.22.
//
#include "pve_module.h"

void ai_play_turn(char *game_state, char player_turn, uint8_t *col_buff) {
    int col_pointer = minmax(game_state);
    if (col_buff[col_pointer] > 0) {
        game_state[MAX_COL * (col_buff[col_pointer] - 1) + col_pointer] = player_turn;
        col_buff[col_pointer]--;
    }
}

bool pve_engine() {
    uint8_t col_buff[7] = {6,6,6,6,6,6,6};
    bool game_end = false;
    bool quit = false;
    char player_turn = 'x';
    char game_state[42];        // create playing board array
    memset(game_state, 0, sizeof(char) * 42);      // 0 -> empty tile, o -> player o, x -> player x
    paint_field();

    while (!game_end && !quit) {    // check for draw
        for (int i = 0; i < 7; ++i) {
            if (col_buff[i] > 0) {
                break;
            } else if (i == 6) {
                change_led_color(LED_PINK);
                activate_led_line(6);
                return false;
            }
        }
        switch (player_turn) {
            case 'x':
                ai_play_turn(game_state, player_turn, col_buff);
                game_end = check_win(game_state);
                if (game_end) {
                    break;
                }
                sleep(1);
                player_turn = 'o';
                break;
            case 'o':
                if (!play_turn(game_state, player_turn, col_buff)) {
                    quit = true;
                    break;
                }
                game_end = check_win(game_state);
                if (game_end) {
                    break;
                }
                player_turn = 'x';
                break;
            default:
                break;
        }
        paint_token(game_state);        // render current game state
    }
    activate_led_line(6);       // signal game end
    if (game_end) {
        draw_win(player_turn);
    } else {
        draw_quit(player_turn);
    }
    return true;
}