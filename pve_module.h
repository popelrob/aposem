//
// Created by robert on 7.5.22.
//

#ifndef APO_PVE_MODULE_H
#define APO_PVE_MODULE_H

#include "graf_engine.h"
#include "ai_engine.h"
#include "defines.h"
#include "pvp_module.h"

bool pve_engine();
// runs the pve engine, returns false if the match results in a draw, true otherwise
void ai_play_turn(char *game_state, char player_turn, uint8_t *col_buff);
// ai plays a turn and marks it in the game_state buffer

#endif //APO_PVE_MODULE_H
