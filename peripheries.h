//
// Created by robert on 17.5.22.
//
#ifndef APOSEM_PERIPHERIES_H
#define APOSEM_PERIPHERIES_H

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "defines.h"

void knobs_init();
// initialize the knobs memory and values
uint8_t red_knob_turn();
// return the current value of the red knob
uint8_t green_knob_turn();
// return the current value of the green knob
uint8_t blue_knob_turn();
// return the current value of the blue knob
uint8_t is_knob_pressed();
// return id of the knob currently pressed (1 - red, 2 - green, 3 - blue, 0 - none)
void change_led_color(uint32_t color);
// turn on the rgb led diodes and set color
void activate_led_line(int rep);
// activate a small sequence of the led line and repeat 'rep' times

#endif //APOSEM_PERIPHERIES_H
